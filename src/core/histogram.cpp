#include "histogram.h"

#include <QDebug>
#include <QPainter>

#include <cmath>

Histogram::Histogram(QImage* image)
{
    R = new QHash<int, int>;
    G = new QHash<int, int>;
    B = new QHash<int, int>;
    L = new QHash<int, int>;
    generate(image);
}

Histogram::~Histogram()
{
    delete R;
    delete G;
    delete B;
    delete L;
}

void Histogram::generate(QImage* image)
{

    int width  = image->width();
    int height = image->height();

    if (image->format() == QImage::Format_Indexed8)
    {
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int color = qRed(image->pixel(x,y)); //gGray byłoby nadmiarowe - w tym formacie qRed = qGreen = qBlue, a qGrey wykonuje dodatkowo niepotrzebne obliczenia, a zwraca to samo
                //qDebug() << Q_FUNC_INFO << "color = " << color;
                if(L->contains(color)){
                    (*L)[color] = (*L)[color] + 1;
                }
                else {
                    L->insert(color, 1);
                }
            }
    }
    else //if (image->format() == QImage::Format_RGB32)
    {
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int colorR, colorG, colorB;
                QRgb color = image->pixel(x,y);
                colorR = qRed(color);
                colorB = qBlue(color);
                colorG = qGreen(color);
                if(R->contains(colorR)){
                    (*R)[colorR] = (*R)[colorR] + 1;
                }
                else {
                    R->insert(colorR, 1);
                }
                if(G->contains(colorG)){
                    (*G)[colorG] = (*G)[colorG] + 1;
                }
                else {
                    G->insert(colorG, 1);
                }
                if(B->contains(colorB)){
                    (*B)[colorB] = (*B)[colorB] + 1;
                }
                else {
                    B->insert(colorB, 1);
                }
            }
    }
}

/** Returns the maximal value of the histogram in the given channel */
int Histogram::maximumValue(Channel selectedChannel = RGB)
{
    int max_value = 0;
    int temp = 0;

    if(selectedChannel == RGB){
        max_value = maximumValue(RChannel);
        temp = maximumValue(GChannel);
        if(temp > max_value)
            max_value = temp;
        temp = maximumValue(BChannel);
        if(temp > max_value)
            max_value = temp;
        return max_value;
    }
    QHash<int, int>* tab = get(selectedChannel);
    for(QHash<int, int>::const_iterator i=tab->begin(); i != tab->end(); i++){
        if(i.value() >= max_value)
            max_value = i.value();
    }
    return max_value;
}


/** Returns a pointer to the given channel QHash<int, int> */
QHash<int, int>* Histogram::get(Channel channel = LChannel)
{
    if (channel==LChannel) return L;
    if (channel==RChannel) return R;
    if (channel==GChannel) return G;
    if (channel==BChannel) return B;

    return 0;
}

/**
 *  Returns a 255 by 100 QImage containing a Histogram for the given channel.
 *  The background is transparent (Alpha 0, RGB=255) */
QImage Histogram::getImage(Channel channel = LChannel, QBrush pen = Qt::gray)
{
    // Create blank QImage and fill it with transparent background:
    QImage histImage(255, 100, QImage::Format_ARGB32);
    histImage.fill(0);
    QPainter painter(&histImage);
    painter.setBrush(Qt::transparent); 
    painter.setPen(Qt::transparent);
    painter.drawRect(0,0,255,100);

    // Calculate the aspect ratio using the maximal value of the color histograms
    int maximum = (channel == LChannel ? maximumValue(LChannel) :  maximumValue(RGB));
    float ratio = 100.0/float(maximum);

    // Preparing the painter:
    painter.setBrush(pen);
    painter.setPen(pen.color());

    int h;
    // Draw histogram
    QHash<int, int>* hist = get(channel);
    QHash<int, int>::const_iterator cit = hist->begin();

    while (cit != hist->end())
    {
        h = 100 - floor(ratio*cit.value());
        painter.drawLine(cit.key(), h, cit.key(), 100);
        ++cit;
    }

    return histImage;
}

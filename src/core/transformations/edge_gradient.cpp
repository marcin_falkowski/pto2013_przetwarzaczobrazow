#include "edge_gradient.h"

EdgeGradient::EdgeGradient(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

EdgeGradient::EdgeGradient(PNM* img) :
    Convolution(img)
{
}

PNM* EdgeGradient::verticalDetection()
{
    return convolute(g_y, RepeatEdge);
}

PNM* EdgeGradient::horizontalDetection()
{
    return convolute(g_x, RepeatEdge);
}

PNM* EdgeGradient::transform()
{
    int width = image->width();
    int height = image->height();
    PNM* newImage = new PNM(width, height, image->format());

    PNM* xImage = horizontalDetection();
    PNM* yImage = verticalDetection();

    if (image->format() == QImage::Format_Indexed8)
    {
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int x_pixel = qRed(xImage->pixel(x,y));
                int y_pixel = qRed(yImage->pixel(x,y));
                int newPix = sqrt(x_pixel*x_pixel + y_pixel*y_pixel);
                newImage->setPixel(x,y, newPix < PIXEL_VAL_MAX ? newPix : 255 );
            }
    }
    else //if (image->format() == QImage::Format_RGB32)
    {
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                QRgb x_pixel = xImage->pixel(x,y);
                QRgb y_pixel = yImage->pixel(x,y);
                int bluex = qBlue(x_pixel);
                int greenx = qGreen(x_pixel);
                int redx = qRed(x_pixel);
                int bluey = qBlue(y_pixel);
                int greeny = qGreen(y_pixel);
                int redy = qRed(y_pixel);
                newImage->setPixel(x,y,
                    qRgb(sqrt(redx*redx+redy*redy), sqrt(greenx*greenx+greeny*greeny), sqrt(bluex*bluex+bluey*bluey))
                );

            }
    }
    return newImage;
}


#include "blur_gaussian.h"
#include <qmath.h>

BlurGaussian::BlurGaussian(PNM* img) :
    Convolution(img)
{
}

BlurGaussian::BlurGaussian(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* BlurGaussian::transform()
{
    emit message("Blurring...");

    int size = getParameter("size").toInt();
    radius = (size/2)+1;
    sigma = getParameter("sigma").toDouble();

    return convolute(getMask(size, Normalize), RepeatEdge);
}

math::matrix<double> BlurGaussian::getMask(int size, Mode)
{
    math::matrix<double> mask(size, size);
    int offset = size/2;
    for(int x=0; x<size; x++){
        for(int y=0; y<size; y++)
            mask(x, y) = getGauss(x-offset, y-offset, sigma);
    }

    return mask;
}

double BlurGaussian::getGauss(int x, int y, double sigma)
{    
    double sigma_sq_x2 = 2*sigma*sigma;
    return qExp(-(x*x+y*y)/sigma_sq_x2)/ sigma_sq_x2; //pomijam pi bo to tylko współczynnik więc nic nie zmienia
}


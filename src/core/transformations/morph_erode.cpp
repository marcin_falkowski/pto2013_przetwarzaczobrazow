#include "morph_erode.h"

MorphErode::MorphErode(PNM* img) :
    MorphologicalOperator(img)
{
}

MorphErode::MorphErode(PNM* img, ImageViewer* iv) :
    MorphologicalOperator(img, iv)
{
}

const int MorphErode::morph(math::matrix<double> window, math::matrix<bool> se)
{
    double max=0.0;
    int size = window.ColNo();
    for(int x=0; x<size; x++){
        for(int y=0; y<size; y++){
            if(se(x,y) && window(x,y) > max ){
                max = window(x, y);
            }
        }
    }

    return max;
}

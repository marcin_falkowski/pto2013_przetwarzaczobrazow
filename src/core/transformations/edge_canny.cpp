#include "edge_canny.h"

#include "blur_gaussian.h"
#include "conversion_grayscale.h"
#include "edge_sobel.h"

EdgeCanny::EdgeCanny(PNM* img) :
    Convolution(img)
{
    initMembers();
}

EdgeCanny::EdgeCanny(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
    initMembers();
}

void EdgeCanny::initMembers()
{
    m_orientation = math::matrix<double>(image->width(), image->height());
    m_magnitude   = math::matrix<double>(image->width(), image->height());
    int i = 0;
    for(;i<=22;i++)
        bucket[i] = 0;
    for(;i<=67;i++)
        bucket[i] = 1;
    for(;i<=112;i++)
        bucket[i] = 2;
    for(;i<=157;i++)
        bucket[i] = 3;
    for(;i<=202;i++)
        bucket[i] = 0;
    for(;i<=247;i++)
        bucket[i] = 1;
    for(;i<=292;i++)
        bucket[i] = 2;
    for(;i<=337;i++)
        bucket[i] = 3;
    for(;i<=360;i++)
        bucket[i] = 0;
}

void getNextEdgePoints(int bucket, QPoint& one, QPoint& two){
    switch(bucket){
    case 0:
        one.setX(0);
        one.setY(-1);
        two.setX(0);
        two.setY(1);
        break;
    case 1:
        one.setX(1);
        one.setY(-1);
        two.setX(-1);
        two.setY(1);
        break;
    case 2:
        one.setX(-1);
        one.setY(0);
        two.setX(1);
        two.setY(0);
        break;
    case 3:
        one.setX(-1);
        one.setY(-1);
        two.setX(1);
        two.setY(1);
        break;
    default:
        break;
    }
}

PNM* EdgeCanny::transform()
{
    int width  = image->width(),
        height = image->height();

    int upper_thresh = getParameter("upper_threshold").toInt(),
        lower_thresh = getParameter("lower_threshold").toInt();

    PNM* newImage = new PNM(width, height, QImage::Format_Indexed8);

    PNM* tmpImage = ConversionGrayscale(image).transform();
    BlurGaussian blurGauss(tmpImage);
    blurGauss.setParameter("size", 3);
    blurGauss.setParameter("sigma", 1.6);
    tmpImage = blurGauss.transform();
    EdgeSobel sobel(tmpImage);
    math::matrix<double>* horizGradient = sobel.rawHorizontalDetection();
    math::matrix<double>* vertiGradient = sobel.rawVerticalDetection();
    for(int x=0; x < width; x++){
        for(int y=0; y < height; y++){
            m_magnitude(x, y) = sqrt((*horizGradient)(x,y)*(*horizGradient)(x,y)+(*vertiGradient)(x,y)*(*vertiGradient)(x,y));
            //problem z zerami w horiz i verti
            m_orientation(x, y) = (int)((atan2((*vertiGradient)(x,y), (*horizGradient)(x,y))/(M_PI))*180 + 360) % 360;
        }
    }
    std::list<QPoint> initialPoints;
    for(int x=1; x< width-1; x++){
        for(int y=1; y < height-1; y++){
            int theta = m_orientation(x,y);
            double m = m_magnitude(x,y);
            QPoint n1, n2;
            getNextEdgePoints(bucket[theta], n1, n2);
            double neigh_1 = m_magnitude(x+n1.x(), y+n1.y());
            double neigh_2 = m_magnitude(x+n2.x(), y+n2.y());
            if(m > neigh_1 && m > neigh_2 && m > upper_thresh){
                newImage->setPixel(x, y, PIXEL_VAL_MIN);
                initialPoints.push_back(QPoint(x,y));
            }
        }
    }
    QPoint neigh_rel_coord[8] = {
        QPoint(1,-1),
        QPoint(1,0),
        QPoint(1,1),
        QPoint(0,1),
        QPoint(-1,1),
        QPoint(-1,0),
        QPoint(-1,1),
        QPoint(0,-1),
    };
    while(!initialPoints.empty()){
        QPoint p = initialPoints.back();
        initialPoints.pop_back();
        int theta = m_orientation(p.x(), p.y());
        QPoint neigh_list[2] = { QPoint(), QPoint() };
        getNextEdgePoints(bucket[theta], neigh_list[0], neigh_list[1]);
        for(int k=0; k < 2; k++){
            QPoint direction = neigh_list[k];
            QPoint current_point = p;
            while(true){
                QPoint next_point = QPoint(current_point.x()+direction.x(), current_point.y()+direction.y());
                if(next_point.x() == width-1 || next_point.x() == 0 || next_point.y() == height-1 || next_point.y() == 0)
                    break;
                if(newImage->pixel(next_point.x(), next_point.y()) == PIXEL_VAL_MIN)
                    break;
                if(m_magnitude(next_point.x(), next_point.y()) < lower_thresh)
                    break;
                if(bucket[(int)m_orientation(next_point.x(), next_point.y())] != bucket[theta])
                    break;
                bool max_magn = true;
                double magn = m_magnitude(next_point.x(), next_point.y());
                for(int i=0;i<8;i++){
                    QPoint tmp_neigh(next_point.x() + neigh_rel_coord[i].x(),
                                     next_point.y() + neigh_rel_coord[i].y());
                    if(tmp_neigh.x() == current_point.x() && tmp_neigh.y() == current_point.y() )
                        continue;
                    if(bucket[(int)m_orientation(tmp_neigh.x(), tmp_neigh.y())] != bucket[theta]){
                        continue;
                    }
                    if(m_magnitude(tmp_neigh.x(), tmp_neigh.y()) >= magn){
                        max_magn = false;
                        break;
                    }
                }
                if(!max_magn)
                    break;
                newImage->setPixel(next_point.x(), next_point.y(), PIXEL_VAL_MIN);
                current_point = next_point;
            }
        }
    }

    return newImage;
}



void EdgeCanny::checkHysterysis(int i, int j, int di, int dj, int direction, int lower_thresh, PNM* newImage)
{
    qDebug() << Q_FUNC_INFO << "Not implemented yet!";
}

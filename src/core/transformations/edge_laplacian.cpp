#include "edge_laplacian.h"

EdgeLaplacian::EdgeLaplacian(PNM* img) :
    Convolution(img)
{
}

EdgeLaplacian::EdgeLaplacian(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

math::matrix<double> EdgeLaplacian::getMask(int, Mode)
{
    int size = getParameter("size").toInt();
    math::matrix<double> mask(size, size);

    for (int x=0; x<mask.RowNo(); x++){
        for(int y=0; y<mask.ColNo(); y++){
            mask(x,y) = -1;
        }
    }
    mask(size/2, size/2) = size*size-1;
    return mask;
}


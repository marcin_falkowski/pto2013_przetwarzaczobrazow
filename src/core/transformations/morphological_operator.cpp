#include "morphological_operator.h"

MorphologicalOperator::MorphologicalOperator(PNM* img) :
    Transformation(img)
{
}

MorphologicalOperator::MorphologicalOperator(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

// abstract
const int MorphologicalOperator::morph(math::matrix<double>, math::matrix<bool>)
{
    return 0;
}

math::matrix<bool> MorphologicalOperator::getSE(int size, SE shape)
{
    switch (shape)
    {
    case Square:    return seSquare(size);
    case Cross:     return seCross(size);
    case XCross:    return seXCross(size);
    case VLine:     return seVLine(size);
    case HLine:     return seHLine(size);
    default:        return seSquare(size);
    }
}


math::matrix<bool> MorphologicalOperator::seSquare(int size)
{
    math::matrix<bool> ret(size, size);
    for(int x=0; x<size; x++){
        for(int y=0; y<size; y++){
            ret(x,y ) = true;
        }
    }
    return ret;
}

math::matrix<bool> MorphologicalOperator::seCross(int size)
{
    math::matrix<bool> ret(size, size);
    for(int x=0; x<size; x++)
        for(int y=0; y<size; y++)
            ret(x,y) = false;

    int mid = size/2;
    for(int x=0; x<size; x++){
        ret(x, mid) = true;
        ret(mid, x) = true;
    }
    return ret;
}

math::matrix<bool> MorphologicalOperator::seXCross(int size)
{
    math::matrix<bool> ret(size, size);
    for(int x=0; x<size; x++)
        for(int y=0; y<size; y++)
            ret(x,y) = false;
    for(int x=0; x<size; x++){
        ret(x, x) = true;
        ret(x, size-x-1) = true;
    }
    return ret;
}

math::matrix<bool> MorphologicalOperator::seVLine(int size)
{
    math::matrix<bool> ret(size, size);
    for(int x=0; x<size; x++)
        for(int y=0; y<size; y++)
            ret(x,y) = false;
    int mid = size/2;
    for(int x=0; x<size; x++){
        ret(x, mid) = true;
    }
    return ret;
}

math::matrix<bool> MorphologicalOperator::seHLine(int size)
{
    math::matrix<bool> ret(size, size);
    for(int x=0; x<size; x++)
        for(int y=0; y<size; y++)
            ret(x,y) = false;
    int mid = size/2;
    for(int x=0; x<size; x++){
        ret(mid, x) = true;
    }
    return ret;
}

PNM* MorphologicalOperator::transform()
{  
    int size  = getParameter("size").toInt();
    SE  shapeE = (MorphologicalOperator::SE) getParameter("shape").toInt();

    int width = image->width();
    int height = image->height();
    PNM* newImage = new PNM(width, height, QImage::Format_RGB32);

    math::matrix<bool> shape = getSE(size, shapeE);


    if(image->format() == QImage::Format_Indexed8){
        for(int x=0; x<width; x++){
            for(int y=0; y<height; y++){
                int val = morph(getWindow(x, y, size, LChannel, RepeatEdge), shape);
                newImage->setPixel(x, y, qRgb(val, val, val));
            }
        }
    }
    else {
        for(int x=0; x<width; x++){
            for(int y=0; y<height; y++){
                newImage->setPixel(
                    x,
                    y,
                    qRgb(
                        morph(getWindow(x, y, size, RChannel, RepeatEdge), shape),
                        morph(getWindow(x, y, size, GChannel, RepeatEdge), shape),
                        morph(getWindow(x, y, size, BChannel, RepeatEdge), shape)
                    )
                );
            }
        }
    }


    return newImage;
}

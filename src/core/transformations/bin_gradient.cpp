#include "bin_gradient.h"
#include "bin_manual.h"
#include "conversion_grayscale.h"

BinarizationGradient::BinarizationGradient(PNM* img) :
    Transformation(img)
{
}

BinarizationGradient::BinarizationGradient(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* BinarizationGradient::transform()
{
    int width = image->width();
    int height = image->height();
    int licznik = 0;
    int mianownik = 0;

    if (image->format() == QImage::Format_RGB32){
        image = ConversionGrayscale(image).transform();
    }

    for(int x=1; x<width-1; x++){
        for(int y=1; y<height-1; y++){
            int gx = qRed( image->pixel(x+1, y) ) - qRed( image->pixel(x+1, y) );
            int gy = qRed( image->pixel(x, y+1) ) - qRed( image->pixel(x, y-1) );
            int g = gx > gy ? gx : gy;

            licznik += qRed(image->pixel(x, y)) * g;
            mianownik += g;
        }
    }

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);
    BinarizationManual::doTransform(image, newImage, licznik/mianownik);
    //qDebug() << Q_FUNC_INFO << "Not implemented yet!";

    return newImage;
}



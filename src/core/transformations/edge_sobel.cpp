#include "edge_sobel.h"

EdgeSobel::EdgeSobel(PNM* img, ImageViewer* iv) :
    EdgeGradient(img, iv)
{
    prepareMatrices();
}

EdgeSobel::EdgeSobel(PNM* img) :
    EdgeGradient(img)
{
    prepareMatrices();
}

void EdgeSobel::prepareMatrices()
{
    g_x.SetSize(3,3);
    g_x(0,0) = -1;
    g_x(0,1) = 0;
    g_x(0,2) = 1;
    g_x(1,0) = -2;
    g_x(1,1) = 0;
    g_x(1,2) = 2;
    g_x(2,0) = -1;
    g_x(2,1) = 0;
    g_x(2,2) = 1;

    g_y.SetSize(3,3);
    g_y(0,0) = -1;
    g_y(0,1) = -2;
    g_y(0,2) = -1;
    g_y(1,0) = 0;
    g_y(1,1) = 0;
    g_y(1,2) = 0;
    g_y(2,0) = 1;
    g_y(2,1) = 2;
    g_y(2,2) = 1;
}

math::matrix<double>* EdgeSobel::rawHorizontalDetection()
{
    math::matrix<double>* x_gradient = new math::matrix<double>(this->image->width(), this->image->height());

    for(int x=x_gradient->RowNo()-1; x>=0; x--){
        for(int y=x_gradient->ColNo()-1; y>=0; y--){
            math::matrix<double> window = getWindow(x, y, 3, LChannel, RepeatEdge);
            (*x_gradient)(x,y) =  sum(join(g_x, window));
        }
    }
    return x_gradient;
}

math::matrix<double>* EdgeSobel::rawVerticalDetection()
{
    math::matrix<double>* y_gradient = new  math::matrix<double>(this->image->width(), this->image->height());

    for(int x=y_gradient->RowNo()-1; x>=0; x--){
        for(int y=y_gradient->ColNo()-1; y>=0; y--){
            math::matrix<double> window = getWindow(x, y, 3, LChannel, RepeatEdge);
            (*y_gradient)(x,y) =  sum(join(g_y, window));
        }
    }

    return y_gradient;
}

#ifndef HISTOGRAM_EQUALIZATION_REAL_H
#define HISTOGRAM_EQUALIZATION_REAL_H

#include "transformation.h"

class HistogramEqualizationReal : public Transformation
{
public:
    HistogramEqualizationReal(PNM*);
    HistogramEqualizationReal(PNM*, ImageViewer*);

    virtual PNM* transform();
};


#endif // HISTOGRAM_EQUALIZATION_REAL_H

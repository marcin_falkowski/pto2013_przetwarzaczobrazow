#include "histogram_equalization.h"

#include "../histogram.h"
#include <QTime>

HistogramEqualization::HistogramEqualization(PNM* img) :
    Transformation(img)
{
}

HistogramEqualization::HistogramEqualization(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* HistogramEqualization::transform()
{
    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    double factor = 255.0/(width*height);

    if (image->format() == QImage::Format_Indexed8)
    {
        QHash<int, int>* Lhist = image->getHistogram()->get(Histogram::LChannel);

        int* dystr = new int[256];
        int curr = 0;
        for(int i=0; i<256; i++){
            curr += Lhist->value(i, 0);
            dystr[i] = curr;
        }
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int color = qRed(image->pixel(x,y)); //gGray byłoby nadmiarowe - w tym formacie qRed = qGreen = qBlue, a qGrey wykonuje dodatkowo niepotrzebne obliczenia, a zwraca to samo
                newImage->setPixel(x, y, round(factor * (color > 0 ? dystr[color-1] : 0)  ));
            }

        delete[] dystr;


    }
    else //if (image->format() == QImage::Format_RGB32)
    {
        QHash<int, int>* Rhist = image->getHistogram()->get(Histogram::RChannel);
        QHash<int, int>* Ghist = image->getHistogram()->get(Histogram::GChannel);
        QHash<int, int>* Bhist = image->getHistogram()->get(Histogram::BChannel);

        int* dystrR = new int[256];
        int curr = 0;
        for(int i=0; i<256; i++){
            curr += Rhist->value(i, 0);
            dystrR[i] = curr;
        }
        int* dystrG = new int[256];
        curr = 0;
        for(int i=0; i<256; i++){
            curr += Ghist->value(i, 0);
            dystrG[i] = curr;
        }
        int* dystrB = new int[256];
        curr = 0;
        for(int i=0; i<256; i++){
            curr += Bhist->value(i, 0);
            dystrB[i] = curr;
        }

        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int colorR, colorG, colorB;
                QRgb color = image->pixel(x,y);
                colorR = qRed(color);
                colorB = qBlue(color);
                colorG = qGreen(color);

                //wprowadzamy losowość aby nie było dziur w histogramie
                colorR = round(factor * (colorR > 0 ? dystrR[colorR-1] : 0));
                colorG = round(factor * (colorG > 0 ? dystrG[colorG-1] : 0));
                colorB = round(factor * (colorB > 0 ? dystrB[colorB-1] : 0));


                newImage->setPixel(x, y, qRgb(colorR, colorG, colorB));
            }
    }

    return newImage;
}


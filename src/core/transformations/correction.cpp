#include "correction.h"

Correction::Correction(PNM* img) :
    Transformation(img)
{
}

Correction::Correction(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* Correction::transform()
{
    double shift  = getParameter("shift").toDouble();
    double factor = getParameter("factor").toDouble();
    double gamma  = getParameter("gamma").toDouble();

    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());


    if (image->format() == QImage::Format_Indexed8)
    {
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int color = qRed(image->pixel(x,y)); //gGray byłoby nadmiarowe - w tym formacie qRed = qGreen = qBlue, a qGrey wykonuje dodatkowo niepotrzebne obliczenia, a zwraca to samo
                color = add(color, shift);
                color = mult(color, factor);
                color = power(color, gamma);
                newImage->setPixel(x, y, color);
            }
    }
    else //if (image->format() == QImage::Format_RGB32)
    {
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int colorR, colorG, colorB;
                QRgb color = image->pixel(x,y);
                colorR = qRed(color);
                colorB = qBlue(color);
                colorG = qGreen(color);
                colorR = add(colorR, shift);
                colorR = mult(colorR, factor);
                colorR = power(colorR, gamma);

                colorB = add(colorB, shift);
                colorB = mult(colorB, factor);
                colorB = power(colorB, gamma);

                colorG = add(colorG, shift);
                colorG = mult(colorG, factor);
                colorG = power(colorG, gamma);
                newImage->setPixel(x, y, qRgb(colorR, colorG, colorB));
            }
    }

    return newImage;
}

const int Correction::add(double value, double shift)
{
    int result = int(value+shift);
    return result > PIXEL_VAL_MAX ? PIXEL_VAL_MAX : result < PIXEL_VAL_MIN ? PIXEL_VAL_MIN : result;
}

const  double Correction::mult(int value, double factor)
{
    double result =  value*factor;
    return result > PIXEL_VAL_MAX ? PIXEL_VAL_MAX : result < PIXEL_VAL_MIN ? PIXEL_VAL_MIN : result;
}

const int Correction::power(int value, double power)
{
    int result =  int(pow(value, power));
    return result > PIXEL_VAL_MAX ? PIXEL_VAL_MAX : result < PIXEL_VAL_MIN ? PIXEL_VAL_MIN : result;
}

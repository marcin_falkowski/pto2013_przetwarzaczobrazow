#include "map_horizon.h"

#include "map_height.h"

MapHorizon::MapHorizon(PNM* img) :
    Transformation(img)
{
}

MapHorizon::MapHorizon(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* MapHorizon::transform()
{
    int width  = image->width(),
        height = image->height();

    double scale     = getParameter("scale").toDouble();
    int    sun_alpha = getParameter("alpha").toInt();
    int dx, dy;

    switch (getParameter("direction").toInt())
    {
    case NORTH: dx = 0; dy = -1; break;
    case SOUTH: dx = 0; dy = 1; break;
    case EAST:  dx = 1; dy = 0; break;
    case WEST:  dx = -1; dy = 0; break;
    default:
        qWarning() << "Unknown direction!";
        dx =  0;
        dy = -1;
    }
    PNM* newImage = new PNM(width, height, QImage::Format_Indexed8);

    PNM* heightMap = MapHeight(image).transform();
    for(int x=0; x < width; x++){
        for(int y=0; y < height; y++){
            double alpha = 0.0;
            int curr_h = qRed(heightMap->pixel(x,y));
            for(int k=x+dx, l=y+dy;
                k < width && l < height && k >= 0 && l >= 0;
                k += dx, l += dy)
            {
                int ray_h = qRed(heightMap->pixel(k,l));
                if(curr_h < ray_h){
                    double dist = sqrt(pow(k-x, 2)+pow(l-y, 2))*scale;
                    double ray_alpha = atan((ray_h-curr_h)/dist)*180./M_PI;
                    if(ray_alpha > alpha)
                        alpha = ray_alpha;
                }
            }
            double delta = alpha - sun_alpha;
            if(delta <= 0)
                newImage->setPixel(x,y, PIXEL_VAL_MAX);
            else
                newImage->setPixel(x, y, cos(delta*M_PI/180.)*PIXEL_VAL_MAX);
        }
    }
    delete heightMap;
    return newImage;
}

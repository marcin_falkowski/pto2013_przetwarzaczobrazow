#include "map_normal.h"

#include "edge_sobel.h"
#include "map_height.h"

MapNormal::MapNormal(PNM* img) :
    Convolution(img)
{
}

MapNormal::MapNormal(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* MapNormal::transform()
{
    int width  = image->width(),
        height = image->height();

    double strength = getParameter("strength").toDouble();
    double dZnorm = 1/strength;

    PNM* newImage = MapHeight(image).transform();
    EdgeSobel sobel(newImage);
    math::matrix<double>* G_x = sobel.rawHorizontalDetection();
    math::matrix<double>* G_y = sobel.rawVerticalDetection();
    delete newImage;
    newImage = new PNM(width, height, QImage::Format_RGB32);
    for(int x=0; x < width; x++){
        for(int y=0; y < height; y++){
            double dX = (*G_x)(x,y)/PIXEL_VAL_MAX;
            double dY = (*G_y)(x,y)/PIXEL_VAL_MAX;
            double length = sqrt( dX*dX + dY*dY + dZnorm*dZnorm );
            dX /= length;
            dY /= length;
            double dZ = dZnorm / length;
            dX = (dX + 1.0)*(255/strength);
            dY = (dY + 1.0)*(255/strength);
            dZ = (dZ + 1.0)*(255/strength);
            newImage->setPixel(x,y, qRgb(dX, dY, dZ));
        }
    }
    return newImage;
}

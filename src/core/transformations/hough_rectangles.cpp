#include "hough_rectangles.h"
#include "hough_lines.h"
#include "corner_harris.h"

HoughRectangles::HoughRectangles(PNM* img) :
    Transformation(img)
{
}

HoughRectangles::HoughRectangles(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

void Edge::remove(){
    for(QLinkedList<Corner*>::iterator it = corners.begin(); it != corners.end(); it++){
        (*it)->edges.removeOne(this);
    }
}


//void removeCorner(&QLinkedList<Corner> list, QLinkedList<Corner>::iterator it){
//
//}


//wyszukiwanie wgłąb kolejnego narożnika prostokąta
void  searchNextCorner(QLinkedList<Corner*> rectCandidate, double last_degree, QLinkedList<Rectangle>* rectList){
    Corner* lastCorner = rectCandidate.back();
    for(QLinkedList<Edge*>::iterator eit = lastCorner->edges.begin(); eit != lastCorner->edges.end(); eit++){
        if(rectCandidate.size() == 1 || ( abs((*eit)->rtheta - last_degree) - M_PI/2 < 0.01)  ){ //znaleziono prostopadłą lub szukamy drugiego wierzch.
            for(QLinkedList<Corner*>::iterator cit = (*eit)->corners.begin(); cit != (*eit)->corners.end(); ){
                if(*cit == lastCorner){
                    continue;
                }
                QLinkedList<Corner*> newRectCand = rectCandidate;
                newRectCand.append(*cit);
                if(rectCandidate.size() == 3){ //szukamy ostatniego
                    if(*cit == rectCandidate.front()){ //znalezlismy ponownie pierwszy wierzcholek
                        rectList->append(Rectangle(rectCandidate));
                        return;
                    }

                }
                else
                    searchNextCorner(newRectCand, (*eit)->rtheta, rectList); //szukamy kolejnego
            }
        }
    }
    if(rectCandidate.size() == 1){ //przez ten wierzcholek juz nie zbudujemy prostokąta
        lastCorner->remove();
    }
}

PNM* HoughRectangles::transform()
{
    HoughLines lines(image);
    const int threshold = 150;
    lines.setParameter("threshold", threshold);
    lines.setParameter("draw_whole_lines", false);

    //znajdujemy krawędzie
    PNM* houghImage;
    PNM* edgesImg = lines.draw(&houghImage);

    //tworzymy listę krawędzi
    QLinkedList<Edge> edgesList;
    for(int theta=0; theta < houghImage->width(); theta++){
        for(int rho=0; rho < houghImage->height(); rho++){
            if(qRed(houghImage->pixel(theta, rho)) > threshold){
                double rtheta = (theta/3.0)*M_PI/180.0;
                int real_rho = rho - houghImage->height()/2;
                edgesList.append(Edge(rtheta, real_rho));
            }
        }
    }

    //znajdujemy narożniki
    CornerHarris cornerDetector(image);
    cornerDetector.setParameter("threshold", 30000000);
    cornerDetector.setParameter("sigma", 1.0);
    cornerDetector.setParameter("sigma_weight", 0.78);
    cornerDetector.setParameter("k", 0.05);
    PNM* cornersImg = cornerDetector.transform();

    //tworzymy listę narożników, z pominięciem tych, które nie są na krawędzi
    QLinkedList<Corner> cornerList;
    for(int x=0; x < cornersImg->width(); x++){
        for(int y=0; y < cornersImg->height(); y++){
            int sum =0;
            for(int i=-2; i < 3; i++){
                for(int j=-2; j < 3; j++){
                    int w = x+i;
                    int z = y+j;
                    if(w < 0 || w >= edgesImg->width() || z < 0 || z >= edgesImg->height() ){
                        continue;
                    }
                    sum += qRed(edgesImg->pixel(w,z));
                }
            }
            if(sum > 0 && qRed( cornersImg->pixel(x, y)) > 0 ){
                cornerList.append(Corner(x,y));
            }
        }
    }

    qDebug() << Q_FUNC_INFO << "cornerList size = " << cornerList.size();

    //przypisujemy punkty krawędziom i na odwrót
    QLinkedList<Corner>::const_iterator cend = cornerList.end();
    QLinkedList<Edge>::const_iterator eend = edgesList.end();
    for(QLinkedList<Edge>::iterator eit = edgesList.begin(); eit != eend; eit++){
        double sinth = sin(eit->rtheta);
        double costh = cos(eit->rtheta);
        double eps = 1;//max(sinth, costh);
        for(QLinkedList<Corner>::iterator cit = cornerList.begin(); cit != cend; cit++ ){
            if(abs(cit->x*costh + cit->y*sinth - eit->rho) < eps){
                qDebug() << Q_FUNC_INFO << "narożnik na lini! ";
                edgesImg->setPixel(cit->x, cit->y, qRgb(255,100,0));
                //narożnik na lini
                cit->edges.append(&(*eit));
                qDebug() << Q_FUNC_INFO << "ten narożnik ma już x krawędzi x= " << cit->edges.size();
                eit->corners.append(&(*cit));
            }
        }
    }

    qDebug() << Q_FUNC_INFO << ". cornerList size = " << cornerList.size();
    qDebug() << Q_FUNC_INFO << ". edgesList size = " << edgesList.size();

    //odrzucamy krawędzie z <2 narożnikami
    for(QLinkedList<Edge>::iterator eit = edgesList.begin(); eit != edgesList.end(); ){
        int corners = eit->corners.size();
        if(corners < 2){
            eit->remove();
            eit = edgesList.erase(eit);
        }
        else
            eit++;
    }

    qDebug() << Q_FUNC_INFO << ".. cornerList size = " << cornerList.size();
    qDebug() << Q_FUNC_INFO << ".. edgesList size = " << edgesList.size();

    //odrzucamy punkty leżące na mniej niż 2 krawędziach
    for(QLinkedList<Corner>::iterator cit = cornerList.begin(); cit != cornerList.end(); ){
        int edges = cit->edges.size();
        if(edges < 2){
            cit->remove();
            cit = cornerList.erase(cit);
        }
        else
            cit++;
    }

    qDebug() << Q_FUNC_INFO << "... cornerList size = " << cornerList.size();
    qDebug() << Q_FUNC_INFO << "... edgesList size = " << edgesList.size();

    QLinkedList<Rectangle> rects;
    /*
    //szukamy "ścieżek" cztero-elementowych z prostopadłymi krawędziami
    for(QLinkedList<Corner>::iterator cit = cornerList.begin(); cit != cornerList.end(); ){
        QLinkedList<Corner*> crnrList;
        crnrList.append(&(*cit));
        searchNextCorner(crnrList, 0, &rects);
    }
    */

    qDebug() << Q_FUNC_INFO << "rects size = " << rects.size();

    return edgesImg;
}


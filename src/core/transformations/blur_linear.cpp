#include "blur_linear.h"

BlurLinear::BlurLinear(PNM* img) :
    Convolution(img)
{
}

BlurLinear::BlurLinear(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* BlurLinear::transform()
{
    int maskSize = getParameter("size").toInt();
    QList<QVariant> tmpMask = getParameter("mask").toList();
    bool normalize = getParameter("normalize").toBool();

    math::matrix<double> mask(maskSize, maskSize);


    for(int x=0; x<maskSize; x++){
        for(int y=0; y<maskSize; y++)
            mask(x, y) = tmpMask.at(y*maskSize+x).toDouble();
    }
    if(normalize){
        double s = sum(mask);
        for(int x=0; x<maskSize; x++){
            for(int y=0; y<maskSize; y++)
                mask(x, y) /= s;
        }
    }

    return convolute(mask, RepeatEdge);
}

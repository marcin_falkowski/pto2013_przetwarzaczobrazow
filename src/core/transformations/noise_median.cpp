#include "noise_median.h"

NoiseMedian::NoiseMedian(PNM* img) :
    Convolution(img)
{
}

NoiseMedian::NoiseMedian(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* NoiseMedian::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());
    if(image->format() == QImage::Format_Indexed8){
        for(int x=0; x<width; x++){
            for(int y=0; y<height; y++){
                newImage->setPixel(x, y, getMedian(x, y, LChannel));
            }
        }
    }
    else {
        for(int x=0; x<width; x++){
            for(int y=0; y<height; y++){
                newImage->setPixel(
                    x,
                    y,
                    qRgb(
                        getMedian(x, y, RChannel),
                        getMedian(x, y, GChannel),
                        getMedian(x, y, BChannel)
                    )
                );
            }
        }
    }

    return newImage;
}

int NoiseMedian::compare(const void* a, const void* b)
{
    return (*(int*)a - *(int*)b);
}

int NoiseMedian::getMedian(int x, int y, Channel channel)
{
    int radius = getParameter("radius").toInt();

    math::matrix<double> window = getWindow(x, y, radius*2+1, channel, RepeatEdge);
    double* buffer = new double[window.ColNo()*window.RowNo()];
    int i=0;
    for(int x=0; x<window.RowNo(); x++){
        for(int y=0; y<window.ColNo(); y++){
            buffer[i++] = window(x, y);
        }
    }
    sort(buffer, buffer + i);
    int median = buffer[i/2];
    delete [] buffer;

    return median;
}

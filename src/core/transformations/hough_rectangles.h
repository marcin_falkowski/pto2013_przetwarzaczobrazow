#ifndef HOUGH_RECTANGLES_H
#define HOUGH_RECTANGLES_H

#include <QLinkedList>
#include "transformation.h"


class HoughRectangles : public Transformation
{
public:
    explicit HoughRectangles(PNM*);
    explicit HoughRectangles(PNM*, ImageViewer*);

    virtual PNM* transform();
};

struct Corner;

struct Edge {
    Edge(double rth, int r) : rtheta(rth), rho(r) {}
    double rtheta;
    int rho;
    QLinkedList<Corner*> corners;
    void remove();
};

struct Corner {
    Corner(int xx, int yy) : x(xx), y(yy) {}
    int x,y;
    QLinkedList<Edge*> edges;
    inline void remove(){
        for(QLinkedList<Edge*>::iterator it = edges.begin(); it != edges.end(); it++){
            (*it)->corners.removeOne(this);
        }
    }
};

struct Rectangle {
    int x1,x2,y1,y2;
    inline Rectangle(QLinkedList<Corner*> corns){
        Corner* front = corns.front();
        Corner* back = corns.back();
        x1 = front->x;
        y1 = front->y;
        x2 = back->x;
        y2 = back->y;
    }
};

#endif // HOUGH_RECTANGLES_H

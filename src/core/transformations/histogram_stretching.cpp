#include "histogram_stretching.h"

#include "../histogram.h"

HistogramStretching::HistogramStretching(PNM* img) :
    Transformation(img)
{
}

HistogramStretching::HistogramStretching(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}


struct IntPair {
    int a;
    int b;
};

IntPair getMinMaxInHistogram(QHash<int, int>* tab){
    int min = INT_MAX;
    int max = -1;
    for(QHash<int, int>::const_iterator i=tab->begin(); i != tab->end(); i++){
        if(i.value() > 0){
            int key = i.key();
            if(key < min)
                min = key;
            if(key > max)
                max = key;
        }
    }
    return { min, max };
}

PNM* HistogramStretching::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    if (image->format() == QImage::Format_Indexed8)
    {
        IntPair bounds = getMinMaxInHistogram(image->getHistogram()->get(Histogram::LChannel));
        double factor = 255.0/(bounds.b - bounds.a);

        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int color = qRed(image->pixel(x,y)); //gGray byłoby nadmiarowe - w tym formacie qRed = qGreen = qBlue, a qGrey wykonuje dodatkowo niepotrzebne obliczenia, a zwraca to samo
                color = round(factor*(color-bounds.a));
                color = (color > 255) ? 255 : (color < 0) ? 0 : color;
                newImage->setPixel(x, y, color);
            }
    }
    else //if (image->format() == QImage::Format_RGB32)
    {
        IntPair boundsR = getMinMaxInHistogram(image->getHistogram()->get(Histogram::RChannel));
        IntPair boundsG = getMinMaxInHistogram(image->getHistogram()->get(Histogram::GChannel));
        IntPair boundsB = getMinMaxInHistogram(image->getHistogram()->get(Histogram::BChannel));
        double factorR = 255.0/(boundsR.b - boundsR.a);
        double factorG = 255.0/(boundsG.b - boundsG.a);
        double factorB = 255.0/(boundsB.b - boundsB.a);
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int colorR, colorG, colorB;
                QRgb color = image->pixel(x,y);
                colorR = qRed(color);
                colorB = qBlue(color);
                colorG = qGreen(color);

                colorR = round(factorR*(colorR-boundsR.a));
                colorG = round(factorG*(colorG-boundsG.a));
                colorB = round(factorB*(colorB-boundsB.a));

                colorR = (colorR > 255) ? 255 : (colorR < 0) ? 0 : colorR;
                colorG = (colorG > 255) ? 255 : (colorG < 0) ? 0 : colorG;
                colorB = (colorB > 255) ? 255 : (colorB < 0) ? 0 : colorB;
                newImage->setPixel(x, y, qRgb(colorR, colorG, colorB));
            }
    }

    return newImage;
}


#include "histogram_stretching_real.h"

#include "../histogram.h"
#include <QTime>

HistogramStretchingReal::HistogramStretchingReal(PNM* img) :
    Transformation(img)
{
}

HistogramStretchingReal::HistogramStretchingReal(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}


struct IntPair {
    int a;
    int b;
};

IntPair getMinMaxInHistogram(QHash<int, int>* tab);

PNM* HistogramStretchingReal::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    qsrand((uint)QTime::currentTime().msec());

    if (image->format() == QImage::Format_Indexed8)
    {
        IntPair bounds = getMinMaxInHistogram(image->getHistogram()->get(Histogram::LChannel));
        //qDebug() << Q_FUNC_INFO << "MIN: " << bounds.a << ", MAX: " << bounds.b;
        double factor = 255.0/(bounds.b - bounds.a);

        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int color = qRed(image->pixel(x,y)); //gGray byłoby nadmiarowe - w tym formacie qRed = qGreen = qBlue, a qGrey wykonuje dodatkowo niepotrzebne obliczenia, a zwraca to samo

                //wprowadzamy losowość aby nie było dziur w histogramie
                color = round((factor*((color-bounds.a)*1000 + (qrand() % 1000 - 500)))/1000);
                color = (color > 255) ? 255 : (color < 0) ? 0 : color;
                newImage->setPixel(x, y, color);
            }
    }
    else //if (image->format() == QImage::Format_RGB32)
    {
        IntPair boundsR = getMinMaxInHistogram(image->getHistogram()->get(Histogram::RChannel));
        IntPair boundsG = getMinMaxInHistogram(image->getHistogram()->get(Histogram::GChannel));
        IntPair boundsB = getMinMaxInHistogram(image->getHistogram()->get(Histogram::BChannel));
        //qDebug() << Q_FUNC_INFO << "R - MIN: " << boundsR.a << ", MAX: " << boundsR.b;
        //qDebug() << Q_FUNC_INFO << "G - MIN: " << boundsG.a << ", MAX: " << boundsG.b;
        //qDebug() << Q_FUNC_INFO << "B - MIN: " << boundsB.a << ", MAX: " << boundsB.b;
        double factorR = 255.0/(boundsR.b - boundsR.a);
        double factorG = 255.0/(boundsG.b - boundsG.a);
        double factorB = 255.0/(boundsB.b - boundsB.a);
        for (int x=0; x<width; x++)
            for (int y=0; y<height; y++)
            {
                int colorR, colorG, colorB;
                QRgb color = image->pixel(x,y);
                colorR = qRed(color);
                colorB = qBlue(color);
                colorG = qGreen(color);

                //wprowadzamy losowość aby nie było dziur w histogramie
                colorR = round((factorR*((colorR-boundsR.a)*1000 + (qrand() % 1000 - 500)))/1000);
                colorG = round((factorG*((colorG-boundsG.a)*1000 + (qrand() % 1000 - 500)))/1000);
                colorB = round((factorB*((colorB-boundsB.a)*1000 + (qrand() % 1000 - 500)))/1000);

                colorR = (colorR > 255) ? 255 : (colorR < 0) ? 0 : colorR;
                colorG = (colorG > 255) ? 255 : (colorG < 0) ? 0 : colorG;
                colorB = (colorB > 255) ? 255 : (colorB < 0) ? 0 : colorB;
                newImage->setPixel(x, y, qRgb(colorR, colorG, colorB));
            }
    }

    return newImage;
}


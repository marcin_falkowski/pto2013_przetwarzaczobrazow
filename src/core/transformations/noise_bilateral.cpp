#include "noise_bilateral.h"
#include <qmath.h>

NoiseBilateral::NoiseBilateral(PNM* img) :
    Convolution(img)
{
}

NoiseBilateral::NoiseBilateral(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* NoiseBilateral::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    if(image->format() == QImage::Format_Indexed8){
        for(int x=0; x<width; x++){
            for(int y=0; y<height; y++){
                newImage->setPixel(x, y, getNeighbours(x, y, LChannel));
            }
        }
    }
    else {
        for(int x=0; x<width; x++){
            for(int y=0; y<height; y++){
                newImage->setPixel(
                    x,
                    y,
                    qRgb(
                        getNeighbours(x, y, RChannel),
                        getNeighbours(x, y, GChannel),
                        getNeighbours(x, y, BChannel)
                    )
                );
            }
        }
    }

    return newImage;
}

int NoiseBilateral::getNeighbours(int x, int y, Channel channel)
{
    int sigma_d = getParameter("sigma_d").toInt();
    int sigma_r = getParameter("sigma_r").toInt();
    int radius = sigma_d;
    math::matrix<double> window = getWindow(x, y, radius*2+1, channel, RepeatEdge);
    int val = 0;
    int k = 0;
    int val_ij = window(window.RowNo()/2, window.ColNo()/2);
    for(int x=0; x<window.RowNo(); x++){
        for(int y=0; y<window.ColNo(); y++){
            int kernels =
                g( qAbs(x - window.RowNo()/2), qAbs(y - window.ColNo()/2), sigma_d )
                * r( window(x,y), val_ij, sigma_r );
                val += window(x,y)*kernels;
                k += kernels;
        }
    }

    return val/k;
}

double NoiseBilateral::g(int x, int y, int sigma_d)
{
    return qExp(-(x*x+y*y)/(2*sigma_d*sigma_d));
}

double NoiseBilateral::r(int v, int v_ij, int sigma_r)
{
    int dv = v - v_ij;
    return qExp(-(dv*dv)/(2*sigma_r*sigma_r));
}

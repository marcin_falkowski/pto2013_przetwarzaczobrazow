#include "hough_lines.h"

#include "bin_gradient.h"
#include "edge_laplacian.h"
#include "hough.h"
#include "morph_erode.h"

#include <QPainter>

HoughLines::HoughLines(PNM* img) :
    Transformation(img)
{
}

HoughLines::HoughLines(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}


PNM* HoughLines::draw(PNM** houghTransformResult){
    // Cut of value from the image;
    int  threshold      = getParameter("threshold").toInt();
    bool drawWholeLines = getParameter("draw_whole_lines").toBool();

    PNM* newImage = new PNM(image->width(), image->height(), QImage::Format_RGB32);
    newImage->fill(QColor(0,0,0));

    EdgeLaplacian lapl(image);
    lapl.setParameter("size", 3);
    PNM* edgeImage = lapl.transform();
    edgeImage = BinarizationGradient(edgeImage).transform();
    Hough hough(edgeImage);
    hough.setParameter("theta_density", 3);
    hough.setParameter("skip_edge_detection", true);
    PNM* tmpImage = hough.transform();
    *houghTransformResult = tmpImage;

    QPainter painter(newImage);
    painter.setPen(QColor(255,255,255));

    //linie pionowe
    for(int rho=0; rho < tmpImage->height(); rho++){
        if(qRed(tmpImage->pixel(0, rho)) > threshold){
            painter.drawLine(rho, 0, rho, tmpImage->height()-1);
        }
    }
    //pozostałe linie
    for(int theta=1; theta < tmpImage->width(); theta++){
        for(int rho=0; rho < tmpImage->height(); rho++){
            if(qRed(tmpImage->pixel(theta, rho)) > threshold){
                double rtheta = (theta/3.0)*M_PI/180.0;
                int real_rho = rho - tmpImage->height()/2;
                double sint = sin(rtheta);
                painter.drawLine(0, (int)real_rho/sint, newImage->width()-1, (int)(real_rho - (newImage->width()-1)*cos(rtheta))/sint);
            }
        }
    }
    if(drawWholeLines == false){
        MorphErode* erode = new MorphErode(edgeImage);
        erode->setParameter("size", 3);
        erode->setParameter("shape", MorphologicalOperator::Square );
        edgeImage = erode->transform();
        for(int x=0; x < newImage->width(); x++){
            for(int y=0; y< newImage->height(); y++){
                if(qRed(edgeImage->pixel(x,y)) == 0){
                    newImage->setPixel(x, y, qRgb(0, 0, 0));
                }
            }
        }
        delete erode;
    }
    return newImage;
}


PNM* HoughLines::transform()
{
    PNM* tmpImage;
    return draw(&tmpImage);
}

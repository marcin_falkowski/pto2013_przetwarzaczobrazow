#ifndef HISTOGRAM_STRETCHING__REAL_H
#define HISTOGRAM_STRETCHING_REAL_H

#include "transformation.h"

class HistogramStretchingReal : public Transformation
{
public:
    HistogramStretchingReal(PNM*);
    HistogramStretchingReal(PNM*, ImageViewer* iv);

    virtual PNM* transform();
};

#endif // HISTOGRAM_STRETCHING_REAL_H

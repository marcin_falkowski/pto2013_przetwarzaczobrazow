#include "bin_manual.h"
#define PIXEL_VAL_MIN 0
#define PIXEL_VAL_MAX 1

BinarizationManual::BinarizationManual(PNM* img) :
    Transformation(img)
{
}

BinarizationManual::BinarizationManual(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* BinarizationManual::transform()
{
    int threshold = getParameter("threshold").toInt();
    int width  = image->width();
    int height = image->height();
    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

    doTransform(image, newImage, threshold);

    return newImage;
}

void BinarizationManual::doTransform(PNM* image, PNM* newImage, int threshold){
    int width  = image->width();
    int height = image->height();
    for(int x=0; x<width; x++){
        for(int y=0; y<height; y++){
            if ( qRed( image->pixel(x, y) ) >= threshold ){
                newImage->setPixel(x, y, PIXEL_VAL_MAX);
            }
            else {
                newImage->setPixel(x, y, PIXEL_VAL_MIN);
            }
        }
    }
}





#include "morph_dilate.h"

MorphDilate::MorphDilate(PNM* img) :
    MorphologicalOperator(img)
{
}

MorphDilate::MorphDilate(PNM* img, ImageViewer* iv) :
    MorphologicalOperator(img, iv)
{
}

const int MorphDilate::morph(math::matrix<double> window, math::matrix<bool> se)
{
    double min = PIXEL_VAL_MAX+1;
    int size = window.ColNo();
    for(int x=0; x<size; x++){
        for(int y=0; y<size; y++){
            if(se(x,y) && window(x,y) < min ){
                min = window(x, y);
            }
        }
    }

    return min;
}

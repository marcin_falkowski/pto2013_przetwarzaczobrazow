#include "edge_zero.h"

#include "edge_laplacian_of_gauss.h"

EdgeZeroCrossing::EdgeZeroCrossing(PNM* img) :
    Convolution(img)
{
}

EdgeZeroCrossing::EdgeZeroCrossing(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

double getMax(math::matrix<double> window){
    double max=0.0;
    int size = window.ColNo();
    for(int x=0; x<size; x++){
        for(int y=0; y<size; y++){
            if(window(x,y) > max ){
                max = window(x, y);
            }
        }
    }

    return max;
}

double getMin(math::matrix<double> window){
    double min=PIXEL_VAL_MAX+1;
    int size = window.ColNo();
    for(int x=0; x<size; x++){
        for(int y=0; y<size; y++){
            if(window(x,y) < min ){
                min = window(x, y);
            }
        }
    }

    return min;
}




PNM* EdgeZeroCrossing::transform()
{
    int width = image->width(),
        height = image->height();

    int    size  = getParameter("size").toInt();
    double sigma = getParameter("sigma").toDouble();
    int    t     = getParameter("threshold").toInt();

    EdgeLaplaceOfGauss laplOfGauss(image);
    laplOfGauss.setParameter("size", size);
    laplOfGauss.setParameter("sigma", sigma);
    PNM* newImage = laplOfGauss.transform();
    Transformation lapl( laplOfGauss.transform() );

    vector<Channel> channels;
    if(image->format() == QImage::Format_Indexed8){
        channels.push_back(LChannel);
    }
    else {
        channels.push_back(RChannel);
        channels.push_back(GChannel);
        channels.push_back(BChannel);
    }
    double result[3];
    int v_0 = 128;
    for (int x=0; x<width; x++)
        for (int y=0; y<height; y++){
            for(int c=0; c<channels.size(); c++){
                math::matrix<double> lwindow = lapl.getWindow(x, y, size, channels[c], RepeatEdge);
                double max = getMax(lwindow);
                double min = getMin(lwindow);
                if(min < v_0 - t && max > v_0 + t){
                    result[c] = lwindow( size/2, size/2 );
                }
                else {
                    result[c] =  0;
                }
            }
            if(image->format() == QImage::Format_Indexed8){
                newImage->setPixel(x, y, result[0]);
            }
            else {
                newImage->setPixel(x, y, qRgb(result[0], result[1], result[2]));
            }
        }
    return newImage;
}

